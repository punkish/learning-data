---
uri: http://workshops.boundlessgeo.com/tutorial-autocomplete/
---

This tutorial will show how to build an autocomplete form field using OpenGeo Suite, and tie the field to a dynamic map view of the selected address. While this example is about autocompleting addresses, the technique can be used for almost any kind of data to create single-entry search forms. Why have separate “record number”, “keyword search”, “author search” fields on a form, when you can have a single field that transparently and quickly provides relevant alternatives no matter what the user chooses to input? Full-text search autocomplete fields are a form-based hammer suitable for almost any data entry nail.
