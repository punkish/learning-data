---
uri: https://www.youtube.com/playlist?list=PLbsFXT1AdiDup-fQGpv3Dcf4t2e8Xhfjp
tags:
    - basics
    - remote sensing
    - video
    - nano
stars: [4,4,5,3,4,5,2,4,5]
---

A series of short presentations on the basics of remote sensing.

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLbsFXT1AdiDup-fQGpv3Dcf4t2e8Xhfjp" frameborder="0" allowfullscreen></iframe>
