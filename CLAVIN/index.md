---
uri: https://clavin.bericotechnologies.com
tags:
    - API
    - geoparsing
stars: [4,4,5,3,4,5,2,4,5]
---

An open source geoparser that enables advanced geospatial analytics on big data by extracting & resolving geographic entities from unstructured text.

CLAVIN (Cartographic Location And Vicinity INdexer) is an award-winning open source software package for document geotagging and geoparsing that employs context-based geographic entity resolution. It extracts location names from unstructured text and resolves them against a gazetteer to produce data-rich geographic entities.

CLAVIN does not simply “look up” location names – it uses intelligent heuristics to identify exactly which “Springfield” (for example) was intended by the author, based on the context of the document. CLAVIN also employs fuzzy search to handle incorrectly-spelled location names, and it recognizes alternative names (e.g., “Ivory Coast” and “Côte d’Ivoire”) as referring to the same geographic entity.
