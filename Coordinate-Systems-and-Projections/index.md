---
uri: https://www.youtube.com/watch?v=HifUBn4APZA
tags: 
    - projections
    - basics
    - video
    - nano
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

Principals on Geographic and Projected Coordinate Systems used in mapping and GIS.