---
uri: https://www.databasic.io/
tags:
    - data munging
    - programming
    - nano
stars: [4,4,5,3,4,5,2,4,5]
---

DataBasic is a suite of easy-to-use web tools for beginners that introduce concepts of working with data. These simple tools make it easy to work with data in fun ways, so you can learn how to find great stories to tell. **WordCounter** analyzes your text and tells you the most common words and phrases. **WTFcsv** tells you WTF is going on with your .csv file. **SameDiff** compares two or more text files and tells you how similar or different they are.
