---
uri: http://dropchop.io
tags:
    - visualization
    - utility
    - programming
    - nano
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

Dropchop is a browser-based GIS. The need for small-scale GIS operations comes up quite frequently in our work, especially for those without much time. Dropchop aims to empower your spatial data by removing complexity. This project is currently a proof-of-concept and explores three hypotheses:

- GIS can be data-first, not operation-first.
- GIS doesn't always require a server.
- GIS is open.

Dropchop uses a good number of modular, operation-specific tools created by an active and welcoming community. Currently, the app uses **Mapbox.js**, **Turf.js**, **jQuery**, **Font Awesome**, **Geoglyphs**, **browser filesaver**, **shp-write**, and is compiled using **Gulp**.

The project is maintained by the great people of [CUGOS](https://cugos.org/), a open geospatial community in the Pacific Northwest.
