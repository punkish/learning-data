---
uri: http://2012books.lardbucket.org/books/geographic-information-system-basics/s04-preface.html
tags:
    - GIS
    - books
    - basics
stars: [4,4,5,3,4,5,2,4,5]
---

*Essentials of Geographic Information Systems* integrates key concepts behind the technology with practical concerns and real-world applications. Recognizing that many potential GIS users are nonspecialists or may only need a few maps, this book is designed to be accessible, pragmatic, and concise. *Essentials of Geographic Information Systems* also illustrates how GIS is used to ask questions, inform choices, and guide policy. From the melting of the polar ice caps to privacy issues associated with mapping, this book provides a gentle, yet substantive, introduction to the use and application of digital maps, mapping, and GIS.