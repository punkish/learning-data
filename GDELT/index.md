---
uri: http://gdeltproject.org
tags:
    - global data
    - machine learning
stars: [4,4,5,3,4,5,2,4,5]
---

## A Global Database of Society

Supported by Google Jigsaw, the GDELT Project monitors the world's broadcast, print, and web news from nearly every corner of every country in over 100 languages and identifies the people, locations, organizations, counts, themes, sources, emotions, counts, quotes, images and events driving our global society every second of every day, creating a free open platform for computing on the entire world.