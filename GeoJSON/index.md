---
uri: http://geojson.org
tags:
    - data format
    - standard
    - programming
    - nano
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

GeoJSON is a format for encoding a variety of geographic data structures.

<pre class='json'><code>{
    type: Feature,
    geometry: {
        type: Point,
        coordinates: [125.6, 10.1]
    }
    properties: {
        name: Dinagat Islands
    }
}</code></pre>

GeoJSON supports the following geometry types: *Point*, *LineString*, *Polygon*, *MultiPoint*, *MultiLineString*, and *MultiPolygon*. Geometric objects with additional properties are *Feature* objects. Sets of features are contained by *FeatureCollection* objects.
