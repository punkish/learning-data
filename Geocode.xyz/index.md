---
uri: https://geocode.xyz
tags:
    - geocoder
    - API
    - geoparsing
stars: [4,4,5,3,4,5,2,4,5]
---

This API provides forward/reverse geocoding, batch geocoding and geoparsing for Europe.

Use Cases: Civic Address Geocoding, Reverse geocoding for fleet management, Microblog feed geoparsing, Address cleanup and standardization, Batch Geocoding and Geoparsing on large bodies of text, Geo Analytics, etc. Eg, OpenWikiMap.org