---
uri: http://www.spatialanalysisonline.com/HTML/index.html
tags:
    - book
    - basics
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

In this 5th Edition by de Smith, Goodchild, and Longley (2015), the authors  address the full spectrum of spatial analysis and associated modeling techniques that are provided within currently available and widely used geographic information systems (GIS) and associated software. Collectively such techniques and tools are often now described as geospatial analysis, although we use the more common form, spatial analysis, in most of our discussions.

The term ‘GIS’ is widely attributed to Roger Tomlinson and colleagues, who used it in 1963 to describe their activities in building a digital natural resource inventory system for Canada (Tomlinson 1967, 1970). The history of the field has been charted in an edited volume by Foresman (1998) containing contributions by many of its early protagonists. [A timeline of many of the formative influences](http://www.casa.ucl.ac.uk/gistimeline/) upon the field up to the year 2000 is available; and is provided by Longley et al. (2010). [Useful background information](http://www.ncgia.buffalo.edu/gishist/) may be found at the GIS History Project website (NCGIA). Each of these sources makes the unassailable point that the success of GIS as an area of activity has fundamentally been driven by the success of its applications in solving real world problems. The web site for this Guide provides companion material focusing on applications. Amongst these are a series of sector‑specific case studies drawing on recent work in and around London (UK), together with a number of international case studies.
