---
uri: http://newmapsplus.uky.edu/map671-module-08-integrating-qgis-cartodb
tags:
    - qgis
    - cartodb
    - GIS
stars: [4,3,4,5,3,2,3,4]
---

In this module we continue our move away from purely-desktop GIS operations to the exciting land of the web and web mapping. Within this module we introduce ourselves to a new key player in the web mapping world: **CartoDB**. We can think broadly of CartoDB as contributing to the backend of our web mapping process, particularly how we store and access our data. However, as we'll see they also provide the ability to quickly make effective data-driven web maps through their web interface, JavaScript library, and web services.
