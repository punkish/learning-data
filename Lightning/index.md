---
uri: http://lightning-viz.org
tags:
    - JavaScript
    - programming
    - raster
    - WebGL
    - geospatial
stars: [4,4,5,3,4,5,2,4,5]
---

![Lightning](/Lightning/logo5.png =50x50)

![viz](/Lightning/viz.jpg =100%x*)

Lightning provides API-based access to reproducible web visualizations.

Turn your data into interactive visualizations, using your favorite language, with or without a server.

All visualizations are npm modules, built with the latest web technologies, and endlessly customizable.
