---
uri: http://muxlab.github.io/map-effects-100/
tags:
    - leafletjs
    - programming
    - nano
    - GIS
stars: [4,4,5,3,4,5,2,4,5]
---

Cool tips to design UI/UX on **Leaflet** maps:

- Fade-in Highlight Style
- Classified Highlight Color
- Rising Popup
- Interlocking Legend
- SVG Marker Animation
- Binding Chart to Map Operations
- Raphael Animation
- Time Animation Play
- Search Nearest with [Turfjs](/lessons#turf_26)
- Hex Grid with Turfjs
- Container Scroll Driven Map Navigation
- Map Driven Container Scroll Navigation
- Social Media Avatar on Map
- Custom Popup with D3
- SVG Border Line Generation
- Responsive Popup with Picture
- Map on Video
