---
uri: http://pubs.usgs.gov/pp/1395/report.pdf
tags: 
    - projections
    - book
    - basics
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

This publication is a major revision of USGS Bulletin 1532, which is titled Map Projections Used by the U.S. Geological Survey. Although several portions are essentially unchanged except for corrections and clarification, there is consider- able revision in the early general discussion, and the scope of the book, originally limited to map projections used by the U.S. Geological Survey, now extends to include several other popular or useful projections. These and dozens of other projections are described with less detail in the forthcoming USGS publication An Album of Map Projections.