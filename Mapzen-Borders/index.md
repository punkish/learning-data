---
uri: https://mapzen.com/data/borders/
tags:
    - geospatial
    - dataset
    - borders
stars: [4,4,5,3,4,5,2,4,5]
---

![borders](/Mapzen-Borders/borders.jpg =100%x*)

Country, region and city boundary data from OpenStreetMap, served monthly

Each country link contains a zipped directory of geojson files, each describing increasingly detailed administrative boundaries. The hierarchy varies by country but in the United States, admin layer 2 describes the national boundary, layer 4 is states, layer 6 is counties, and layer 8 is cities. The OSM wiki has specifics for each country.

Full planet file is also available.
