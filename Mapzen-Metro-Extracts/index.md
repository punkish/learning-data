---
uri: https://mapzen.com/data/metro-extracts/
tags:
    - geospatial
    - dataset
    - metro
stars: [4,4,5,3,4,5,2,4,5]
---

![metro extracts](/Mapzen-Metro-Extracts/metro-extracts.jpg =100%x*)

City-sized portions of OpenStreetMap, served weekly

Now it’s even easier to get local data so you can start building cool stuff. Each week, we automatically extract the latest OpenStreetMap data into manageable, metro-area shapefiles in a variety of formats for you to use.
