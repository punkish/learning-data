---
uri: https://mapzen.com/projects/mobile/
tags:
    - geospatial
    - SDK
    - mobile
    - Android
    - Mapzen
stars: [4,4,5,3,4,5,2,4,5]
---

![mobile](/Mapzen-Mobile/mobile.jpg =100%x*)

Mobile is how the physical world connects to the internet. We are building a set of cross platform SDKs to simplify integration of Mapzen services into your mobile applications. These SDKs will simplify setup, installation, API key management, and generally make your life better.
