---
uri: https://mapzen.com/projects/search/
tags:
    - geospatial
    - data API
    - search
    - Mapzen
stars: [4,4,5,3,4,5,2,4,5]
---

![search](/Mapzen-Search/search.jpg =100%x*)

Mapzen Search is a search engine for places worldwide, powered by open data. It turns addresses and place names into geographic coordinates, and turns geographic coordinates into places and addresses. With Mapzen Search, you’re able to turn your users’ place searches into actionable geodata and transform your geodata into real places.

Not only is Mapzen Search completely powered by open data, but it is available to everyone, and importantly open to all to make better. Powered by Mapzen’s open source geocoding project, Pelias, we’re building it in the open using Node.js and Elasticsearch. It is open to collaboration from all to make searching our world better and smarter.
