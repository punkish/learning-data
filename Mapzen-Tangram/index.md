---
uri: https://mapzen.com/projects/tangram/
tags:
    - geospatial
    - renderer
    - map design
    - programming
stars: [4,4,5,3,4,5,2,4,5]
---

![Tangram](/Mapzen-Tangram/tangram.jpg =100%x*)

Tangram is an open-source map renderer designed to grant you ludicrous levels of control over your map design. By drawing vector tiles live in a web browser or mobile device, it allows real-time map design, display, and interactivity.

Using OpenGL, Tangram saddles and rides your graphics card into a new world of cartographic exploration. Animated shaders, 3D buildings, and dynamic filtering can be combined to produce effects normally seen only in science fiction.

Map styles, data filters, labels, and even graphics card code can be defined in a human-readable and -writable plaintext scene file, and APIs permit direct interactive control of the style.

Tangram is available in two delicious flavors: Tangram JS for browser-based web maps, and Tangram ES for mobile applications.
