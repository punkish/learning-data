---
uri: https://transit.land
tags:
    - geospatial
    - dataset
    - transit
    - Mapzen
stars: [4,4,5,3,4,5,2,4,5]
---

![Transitland logo](/Mapzen-Transitland/logo.png =100%x*)

A COMMUNITY-EDITED DATA SERVICE
AGGREGATING TRANSIT NETWORKS
ACROSS METROPOLITAN AND RURAL
AREAS AROUND THE WORLD.

Transitland is aggregating existing geographic and temporal data from authoritative sources; we're connecting the dots using common identifiers; and we're equipping ourselves, our collaborators, and perhaps also you to edit and improve this "community datastore" with knowledge from boots on the ground.
