---
uri: https://mapzen.com/projects/vector-tiles/
tags:
    - geospatial
    - dataset
    - vector tiles
    - Mapzen
stars: [4,4,5,3,4,5,2,4,5]
---

![vector tile](/Mapzen-Vector-Tiles/vector-tile.jpg =100%x*)

Mapzen’s vector tile service delivers worldwide coverage of OpenStreetMap base layer data with up-to-the-minute updates, and is available in GeoJSON, TopoJSON, and binary formats (Mapbox and OpenScienceMap). Our vector tiles can be displayed via a number of open technologies, including SVG/d3, Canvas and WebGL.

Simply put, vector tiles are squares of math. Instead of a web server sending you a pre-drawn image tile, a vector tile contains the OSM geometry for a particular part of the earth, delivered on demand. It contains instructions on what can be drawn, rather than something already drawn and styled. Above is a snippet of a vector tile in GeoJSON; as you can see it contains mathematical descriptions and metadata of buildings, roads and other features in the OSM base layer.
