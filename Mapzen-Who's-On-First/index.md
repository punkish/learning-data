---
uri: http://whosonfirst.mapzen.com
tags:
    - geospatial
    - dataset
    - gazetteer
stars: [4,4,5,3,4,5,2,4,5]
---

Who's On First is a gazetteer of places. Not quite all the places in the world but a whole lot of them and, we hope, the kinds of places that we mostly share in common.

*A gazetteer is a big list of places, each with a stable identifier and some number of descriptive properties about that location.*

An interesting way to think about a gazetteer is to consider it as the space where debate about a place is managed but not decided. Which sort of sums up the problem of geo, nicely. It might be easier, perhaps, if we all understood and experienced the world as coordinate data but we don’t, so the burden of “place” and its many meanings is one we trundle along with to this day.
