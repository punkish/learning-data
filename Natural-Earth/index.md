---
uri: http://www.naturalearthdata.com
tags:
    - geospatial
    - dataset
stars: [4,4,5,3,4,5,2,4,5]
---

![Natural Earth](/Natural-Earth/NEV-Logo-Black.png =100%x*)

![Map Gallery](/Natural-Earth/map.jpg =100%x*)

Natural Earth is a public domain map dataset available at 1:10m, 1:50m, and 1:110 million scales. Featuring tightly integrated vector and raster data, with Natural Earth you can make a variety of visually pleasing, well-crafted maps with cartography or GIS software.

Natural Earth was built through a collaboration of many volunteers and is supported by NACIS (North American Cartographic Information Society), and is free for use in any type of project.
