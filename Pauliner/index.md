---
uri: http://pauliner.punkish.org
tags:
    - API
    - geoparsing
stars: [4,4,5,3,4,5,2,4,5]
---

Pauliner is a naive geoparser, but you may be intrigued by its presumptuousness

Pauliner uses [natural](https://github.com/NaturalNode/natural) to tokenize the input, then match nouns against a dump of [Geonames](http://geonames.org/) (right now using only German place names).

![Pauliner](/Pauliner/pauliner.jpg =320x439) 