---
uri: https://postgresapp.com
tags:
    - geospatial
    - database
    - rdbms
stars: [4,4,5,3,4,5,2,4,5]
---

![Postgresapp](/Postgres.app/postgresapp.jpg =70%x*) 
## Postgres.app
#### The easiest way to get started with PostgreSQL on the Mac

Postgres.app is a full-featured PostgreSQL installation packaged as a standard Mac app. It includes everything you need to get started: we’ve even included popular extensions like [PostGIS](http://postgis.net/) for geo data and [plv8](https://github.com/plv8/plv8) for Javascript.

Postgres.app has a beautiful user interface and a convenient menu bar item. You never need to touch the command line to use it – but of course we do include all the necessary [command line tools](https://postgresapp.com/documentation/cli-tools.html) and header files for advanced users.

Postgres.app updates automatically, so you get bugfixes as soon as possible.

The current version requires macOS 10.10 or later and comes with PostgreSQL versions 9.5 and 9.6, but we also maintain [other versions](https://postgresapp.com/documentation/all-versions.html) of Postgres.app.