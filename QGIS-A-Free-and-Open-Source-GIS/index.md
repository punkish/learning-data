---
uri: http://qgis.org/en/site/
tags:
    - desktop
    - GIS
stars: [4,4,5,3,4,5,2,4,5]
---

![QGIS](/QGIS-A-Free-and-Open-Source-GIS/about-screenshot.png =100%x*)

QGIS, or Quantum GIS, is a free and open source GIS software that let's you create, edit, visualize, analyze and publish geospatial information on Windows, Mac, Linux, BSD (Android coming soon). It is available on the desktop, server, in the web browser and as developer libraries.

QGIS provides a continously growing number of capabilities provided by core functions and plugins. You can visualize, manage, edit, analyse data, and compose printable maps.