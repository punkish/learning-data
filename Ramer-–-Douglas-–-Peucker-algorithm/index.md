---
uri: https://en.wikipedia.org/wiki/Ramer–Douglas–Peucker_algorithm
tags:
    - wikipedia
    - algorithm
    - thinning
    - generalization
    - nano
    - GIS
stars: [2,3,1,1,2,1,1,2]
---

![animation](/Ramer-–-Douglas-–-Peucker-algorithm/Douglas-Peucker_animated.gif =100%x*)

The **Ramer–Douglas–Peucker** algorithm (RDP) is an algorithm for reducing the number of points in a curve that is approximated by a series of points. The initial form of the algorithm was independently suggested in 1972 by Urs Ramer and 1973 by David Douglas and Thomas Peucker and several others in the following decade. This algorithm is also known under the names **Douglas–Peucker** algorithm, **iterative end-point fit** algorithm and **split-and-merge** algorithm.
