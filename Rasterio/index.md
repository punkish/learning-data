---
uri: https://github.com/mapbox/rasterio
tags:
    - geospatial
    - programming
    - Python
    - Numpy
    - GDAL
    - vector tiles
stars: [4,4,5,3,4,5,2,4,5]
---

Rasterio reads and writes geospatial raster datasets.

Rasterio employs GDAL under the hood for file I/O and raster formatting. Its functions typically accept and return Numpy ndarrays. Rasterio is designed to make working with geospatial raster data more productive and more fun.

Rasterio is pronounced raw-STEER-ee-oh.

## Example

Here's a simple example of the basic features rasterio provides. Three bands are read from an image and summed to produce something like a panchromatic band. This new band is then written to a new single band TIFF.

<pre class='python'><code>import numpy
import rasterio

# Read raster bands directly to Numpy arrays.
#
with rasterio.open('tests/data/RGB.byte.tif') as src:
    r, g, b = src.read()

# Combine arrays in place. Expecting that the sum will
# temporarily exceed the 8-bit integer range, initialize it as
# a 64-bit float (the numpy default) array. Adding other
# arrays to it in-place converts those arrays "up" and
# preserves the type of the total array.
total = numpy.zeros(r.shape)
for band in r, g, b:
    total += band
total /= 3

# Write the product as a raster band to a new 8-bit file. For
# the new file's profile, we start with the meta attributes of
# the source file, but then change the band count to 1, set the
# dtype to uint8, and specify LZW compression.
profile = src.profile
profile.update(dtype=rasterio.uint8, count=1, compress='lzw')

with rasterio.open('example-total.tif', 'w', **profile) as dst:
    dst.write(total.astype(rasterio.uint8), 1)
</code></pre>

The output:

![output](/Rasterio/output.jpg =100%x*)
