---
uri: https://github.com/Toblerity/Shapely
tags:
    - Python
    - programming
    - intermediate
    - Shapefile
    - CLI
stars: [4,4,5,3,4,5,2,4,5]
---

Shapely is a BSD-licensed Python package for manipulation and analysis of planar geometric objects. It is based on the widely deployed **GEOS** (the engine of PostGIS) and JTS (from which GEOS is ported) libraries. Shapely is not concerned with data formats or coordinate systems, but can be readily integrated with packages that are.

![patches](/Shapely/patches.png =100%x*)

Here is the canonical example of building an approximately circular patch by buffering a point.

<pre class="code">
>>> from shapely.geometry import Point
>>> patch = Point(0.0, 0.0).buffer(10.0)
>>> patch
<shapely.geometry.polygon.Polygon object at 0x...>
>>> patch.area
313.65484905459385
</pre>
