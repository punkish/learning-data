---
uri: http://gis4geomorphology.com/roughness-topographic-position/
tags: 
    - geomorphology
    - terrain
    - GIS
stars: [3,3,4,5,3,2,4,3,2]
---

Topographic roughness (ruggedness) is defined differently depending on the calculation used. Topographic roughness may be based on standard deviation of slope, standard deviation of elevation, slope convexity, variability of plan convexity (contour curvature), or some other measure of topographic texture. Scale is important in any roughness analysis – both that of the DEM and that of the landscape features you intend to characterize. In many cases, starting with a smoothed DEM produces a more interpretable roughness raster (see Relative Topographic Position). Here are several ways to calculate topographic roughness.