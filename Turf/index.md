---
uri: http://turfjs.org/
tags:
    - JavaScript
    - programming
    - intermediate
    - geospatial
stars: [4,4,5,3,4,5,2,4,5]
---

![TINning](/Turf/tin.png =100%x*)

Turfjs provides advanced geospatial analysis for browsers and node. It provides tools that are:

**Simple:** Modular, simple-to-understand JavaScript functions that speak GeoJSON

**Fast:** Takes advantage of the newest algorithms and doesn't require you to send data to a server

**Modular:** Turf is a collection of small modules. You can require just what you use with `browserify`
