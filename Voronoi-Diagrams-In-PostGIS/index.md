---
uri: http://punkish.org/Voronoi-Diagrams-In-PostGIS
tags:
    - geospatial
    - programming
    - PostGIS
    - PL/R
stars: [4,4,5,3,4,5,2,4,5]
---

A Voronoi diagram represents proximity information about a set of points. The points on the Voronoi diagram are equidistant to two or more sites. Per [Voronoi Diagrams and Delaunay Triangulation](http://www.comp.lancs.ac.uk/~kristof/research/notes/voronoi/)

> Voronoi diagrams were first discussed by Peter Lejeune-Dirichlet in 1850. But it was more than a half of a century later in 1908 that these diagrams were written about in a paper by Voronoi, hence the name Voronoi Diagrams. The Voronoi cells/polygons are sometimes also called Thiessen Polytopes or Dirichlet Regions.

![Voronoi Diagrams](/Voronoi-Diagrams-In-PostGIS/macrostrat.jpg =100%x*)

Here we create Voronoi Diagrams with the help of R embedded inside PostGIS via PL/R.
