---
uri: http://www.macwright.org/mapschool/
tags: 
    - basics
    - GIS
stars: [2,3,1,4,2,3,2,4,2,3,2,5]
---

What is a map? Until the 1980s, maps were painstaking documents created by hand. These days maps are almost always made with the help of a computer. Maps today are commonplace, interspersed in driving directions, visualizations, and political boundary disputes. Let’s look deeper and think about the fundamental elements of maps from the eye of the creator.