---
uri: https://github.com/mbloch/mapshaper
tags:
    - web app
    - programming
    - intermediate
    - Shapefile
    - CLI
stars: [4,4,5,3,4,5,2,4,5]
---

mapshaper is software for editing **Shapefile**, **GeoJSON**, **TopoJSON** and several other data formats, written in JavaScript.

The mapshaper command line program supports essential map making tasks like simplifying shapes, editing attribute data, clipping, erasing, dissolving, filtering and more.

The web UI supports interactive simplification and attribute data editing. There is also a console for running cli commands.
